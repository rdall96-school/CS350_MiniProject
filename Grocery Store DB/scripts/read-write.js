function readXML(filename, callback) {
    
    // get xml file
    xmlhttp = new XMLHttpRequest();
    xmlhttp.externCallback = callback;
    xmlhttp.onload = function() {
        var xmlDoc = new DOMParser().parseFromString(xmlhttp.responseText,'text/xml');

        // create json object from xml file and get all items from it
        var databaseObj = JSON.parse(xml2json(xmlDoc, "\t"));

        // use the database in some external code
        xmlhttp.externCallback(databaseObj);
    }

    xmlhttp.open("GET", filename, true);
    xmlhttp.send();
}

function writeXML(obj, filepath) {
    var xmlText = json2xml(obj, "\t");

    console.log(xmlText);
}

function sortByName(a, b) {
    if (a.name < b.name) {
        return -1;
    }
    if (a.name > b.name) {
        return 1;
    }
    return 0;
}

function sortByPriceLow(a, b) {
    return a.price - b.price;
}

function sortByPriceHigh(a, b) {
    return b.price - a.price;
}

function generateItemCards(dbObj) {
    // loop through every item
    for(var i = 0; i < dbObj.db.items.item.length; i++) {
        // add card to display layout "Inventory"
        document.getElementById("Inventory").innerHTML += createItemCardHTML(
            dbObj.db.items.item[i].name, 
            dbObj.db.items.item[i].price, 
            dbObj.db.items.item[i].image
        );
    }
}

function generateCategoryCards(dbObj) {
    //loop over all categories
    for (var i = 0; i < dbObj.db.categories.category.length; i++) {
        //add a HTML card for each category in the list
        document.getElementById("Inventory").innerHTML += createCategoryCardHTML(
            dbObj.db.categories.category[i].id,
            dbObj.db.categories.category[i].name, 
            dbObj.db.categories.category[i].isle, 
            dbObj.db.categories.category[i].image
        );
    }
}
function generateIsleCards(dbObj) {
    //loop over all isles
    for (var i = 0; i < dbObj.db.isles.isle.length; i++) {
        //add a HTML card for each isle in the list
        document.getElementById("Inventory").innerHTML += createIsleCardHTML(
            dbObj.db.isles.isle[i].id, 
            dbObj.db.isles.isle[i].description
        );
    }
}

function clearCards() {
    document.getElementById("Inventory").innerHTML = "";
}

function filterItemsByCategory(dbObj, category) {
    // get all items in the selected category
    for(var i = 0; i < dbObj.db.items.item.length; i++) {
        // check if item category matches the one selected
        if (dbObj.db.items.item[i].category == category) {
            // add card to display layout "Inventory"
            document.getElementById("Inventory").innerHTML += createItemCardHTML(
                dbObj.db.items.item[i].name, 
                dbObj.db.items.item[i].price, 
                ("../" + dbObj.db.items.item[i].image)
            );
        }
    }
}

function filterItemsByIsle(dbObj, isleNum) {
    //loop through all categories
    for (var i = 0; i < dbObj.db.categories.category.length; i++) {
        //if category isle is equal to isleNum
        if (dbObj.db.categories.category[i].isle == isleNum) {
            //filter items by that category name, and add them to the page
            filterItemsByCategory(dbObj, dbObj.db.categories.category[i].name);
        }
    }
}

function createItemCardHTML(itemName, itemPrice, itemImage) {
    var htmlCode = ['<div class="itemCard"><img src="', 
                    '"/><div class="cardContainer"><h4><b>', 
                    '</b></h4><p>', 
                    '</p></div></div>'];

    var cardCodeString = "";
    cardCodeString += htmlCode[0];
    cardCodeString += itemImage;
    cardCodeString += htmlCode[1];
    cardCodeString += itemName;
    cardCodeString += htmlCode[2];
    cardCodeString += ("$ " + itemPrice);
    cardCodeString += htmlCode[3];
    return cardCodeString;
}

function createCategoryCardHTML(catID, catName, catIsle, catImage) {
    var htmlCode = ['<div class="categoryCard" onclick="readXML(',
                    "'../inventory.xml', function(dbObj) { clearCards(); filterItemsByCategory(dbObj, '",
                    "');})",
                    '">',
                    '<img src="../item_images/', 
                    '"/><div class="cardContainer"><h4><b>', 
                    '</b></h4><p>', 
                    '</p></div></div>'];

    var cardCodeString = "";
    cardCodeString += htmlCode[0];
    cardCodeString += htmlCode[1];
    cardCodeString += catName;
    cardCodeString += htmlCode[2];
    cardCodeString += htmlCode[3];
    cardCodeString += htmlCode[4];
    cardCodeString += catImage;
    cardCodeString += htmlCode[5];
    cardCodeString += (catName.charAt(0).toUpperCase() + catName.slice(1));
    cardCodeString += htmlCode[6];
    cardCodeString += ('Isle: ' + catIsle);
    cardCodeString += htmlCode[7];
    return cardCodeString;
}

function createIsleCardHTML(ID, description) {
    var htmlCode = ['<div class="categoryCard" onclick="readXML(',
                    "'../inventory.xml', function(dbObj) { clearCards(); filterItemsByIsle(dbObj, '",
                    "');})",
                    '">',
                    '<div class="cardContainer"><h4><b>', 
                    '</b></h4><p>', 
                    '</p></div></div>'];

    var cardCodeString = "";
    cardCodeString += htmlCode[0];
    cardCodeString += htmlCode[1];
    cardCodeString += ID;
    cardCodeString += htmlCode[2];
    cardCodeString += htmlCode[3];
    cardCodeString += htmlCode[4];
    cardCodeString += ("Isle # " + ID);
    cardCodeString += htmlCode[5];
    cardCodeString += description;
    cardCodeString += htmlCode[6];
    return cardCodeString;
}

function filterItemsBySearch(dbObj, searchString) {
    // searches through every item name containing the 'searchString'

    // create regex for searchString
    var re = new RegExp(searchString, "i");

    //append search result string to id 'Inventory' in html
    document.getElementById("Inventory").innerHTML += ('<p>Showing results for "' + searchString + '"</p>');

    for(var i = 0; i < dbObj.db.items.item.length; i++) {
        if(dbObj.db.items.item[i].name.search(re) > -1) {
            // create item card
            document.getElementById("Inventory").innerHTML += createItemCardHTML(
                dbObj.db.items.item[i].name, 
                dbObj.db.items.item[i].price, 
                (dbObj.db.items.item[i].image)
            );
        }
    }
}
