function readXML(filename, callback) {
    
    // get xml file
    xmlhttp = new XMLHttpRequest();
    xmlhttp.externCallback = callback;
    xmlhttp.onload = function() {
        var xmlDoc = new DOMParser().parseFromString(xmlhttp.responseText,'text/xml');

        // create json object from xml file and get all items from it
        var databaseObj = JSON.parse(xml2json(xmlDoc, "\t"));

        // use the database in some external code
        xmlhttp.externCallback(databaseObj);
    }

    xmlhttp.open("GET", filename, true);
    xmlhttp.send();
}

function writeXML(obj, filepath) {
    var xmlText = json2xml(obj, "\t");

    console.log(xmlText);
}

function sortData(databaseObj, sortFunction) {
    databaseObj.items.sort(sortFunction);
}

function generateCards(itemList) {

    var i, name, price, image, cardCode;

    // loop through every item
    for(i = 0; i < itemList.items.item.length; i++) {

        // get the name, price and image
        name = itemList.items.item[i].name;
        price = itemList.items.item[i].price;
        image = itemList.items.item[i].image;

        // generate card code for each one
        cardCode = createCardHTML(name, price, image);

        // add card to display layout "Inventory"
        document.getElementById("Inventory").innerHTML += cardCode;
    }

    return;
}

function createCardHTML(itemName, itemPrice, itemImage) {
    var htmlCode = ['<div class="itemCard"><img src="item_images/', '"/><div class="cardContainer"><h4><b>', '</b></h4><p>', '</p></div></div>'];
    var cardCodeString = "";
    cardCodeString += htmlCode[0];
    cardCodeString += itemImage;
    cardCodeString += htmlCode[1];
    cardCodeString += itemName;
    cardCodeString += htmlCode[2];
    cardCodeString += ("$ " + itemPrice);
    cardCodeString += htmlCode[3];
    return cardCodeString;
}