function readXML(filepath, callback) {
    
    xmlhttp = new XMLHttpRequest();
    xmlhttp.externCallback = callback;
    xmlhttp.onload = function() {
        var xmlDoc = new DOMParser().parseFromString(xmlhttp.responseText,'text/xml');

        //console.log(xmlDoc);

        var result = xml2json(xmlDoc, "\t");
        var obj = JSON.parse(result);
        console.log(obj);

        //console.log(obj);
        //console.log(obj.items.item[0].name);

        xmlhttp.externCallback(obj);
    }


    xmlhttp.open("GET", filepath, true);
    xmlhttp.send();
}

function writeXML(obj, filepath) {
    var xmlText = json2xml(obj, "\t");

    console.log(xmlText);
}

function createCardHTML(itemName, itemPrice, itemImage) {
    var htmlCode = ['<div class="itemCard"><img src="item_images/', '"/><div class="cardContainer"><h4><b>', '</b></h4><p>', '</p></div></div>'];
    var cardCodeString = "";
    cardCodeString += htmlCode[0];
    cardCodeString += itemImage;
    cardCodeString += htmlCode[1];
    cardCodeString += itemName;
    cardCodeString += htmlCode[2];
    cardCodeString += itemPrice;
    cardCodeString += htmlCode[3];
    return cardCodeString;
}